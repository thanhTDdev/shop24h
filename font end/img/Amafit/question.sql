-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th7 05, 2021 lúc 12:36 PM
-- Phiên bản máy phục vụ: 10.4.19-MariaDB
-- Phiên bản PHP: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `question`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `answers`
--

CREATE TABLE `answers` (
  `id` bigint(20) NOT NULL,
  `answer_checked` bit(1) DEFAULT NULL,
  `content_answer` varchar(255) NOT NULL,
  `explaination` varchar(255) NOT NULL,
  `question_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `answers`
--

INSERT INTO `answers` (`id`, `answer_checked`, `content_answer`, `explaination`, `question_id`) VALUES
(1, b'0', 'had helped', 'haha', 1),
(2, b'1', 'would have helped', 'haha', 1),
(3, b'0', 'would help', 'haha', 1),
(4, b'0', 'help', 'haha', 2),
(5, b'0', 'wouldn\'t take', 'haha', 1),
(6, b'0', 'wouldn\'t have taken', 'haha', 2),
(7, b'0', 'didn\'t take', 'haha', 2),
(8, b'1', 'hadn\'t taken', 'haha', 2),
(19, b'0', 'had helped', 'haha', 3),
(20, b'1', 'helped', 'haha', 3),
(21, b'0', 'helped', 'haha', 3),
(25, b'1', 'had helped', 'haha', 1),
(26, b'0', 'had helped', 'djadadakđk;d', 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `questions`
--

CREATE TABLE `questions` (
  `id` bigint(20) NOT NULL,
  `content_question` varchar(255) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `mark` float DEFAULT NULL,
  `subject` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `questions`
--

INSERT INTO `questions` (`id`, `content_question`, `create_date`, `mark`, `subject`) VALUES
(1, 'He says he\'s your friend, but he didn\'t help you. If he were your friend, he _____ you.', '2021-01-15 00:00:00', 4, 'English'),
(2, 'I feel fine because I took the medicine. If I _____ the medicine, I would still be in pain.', '2021-07-01 15:41:36', 3, 'English'),
(3, 'He says he\'s your friend, but he didn\'t help you. If he were your friend, he _____ you.', '2021-01-20 00:00:00', 3, 'English'),
(5, 'He says he\'s your friend, but he didn\'t help you. If he were your friend, he _____ you.', '2021-01-20 00:00:00', 5, 'English'),
(23, 'He says he\'s your friend, but he didn\'t help you. If he were your friend, he _____ you.', '2021-01-09 00:00:00', 10, 'English'),
(24, 'He says he\'s your friend, but he didn\'t help you. If he were your friend, he _____ you.', '2021-01-09 00:00:00', 3, 'English');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK3erw1a3t0r78st8ty27x6v3g1` (`question_id`);

--
-- Chỉ mục cho bảng `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `FK3erw1a3t0r78st8ty27x6v3g1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
