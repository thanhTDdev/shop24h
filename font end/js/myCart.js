$(document).ready(function () {
    $("#check_out").hide();
    var gTotalPrice = 0;
    gItem = {
        "quantity": 0,
        "pOrderCode": "",
        "productName": "",
        "productPrice": 0,
        "productImg": ""
    }
    var gCustomer = {
        "lastname": "",
        "firsrname": "",
        "phoneNumber": "",
        "address": "",
        "city": "",
        "email": "",
        "country": ""      
    }
    var gOrders = {
        "status": "",
        "comment": "",
        "amount":0
    }
    var jsonObj = window.localStorage.getItem("items");
    // check your Cart
    if (jsonObj.length === 0) {
        var gObjItem = [];
        $(".qty_cart").hide();
        $(".cart-list").hide();
        //$(".cart-btns").hide();
    } else {
        var gObjItem = JSON.parse(jsonObj);
        LoadItemOnCart(gObjItem)
        showOderItem(gObjItem);
    }
    //load item on Cart
    function LoadItemOnCart(gObjItem) {
        var totalItem = 0;
        for (let i = 0; i < gObjItem.length; i++) {
            totalItem = totalItem + parseInt(gObjItem[i].quantity);
            gTotalPrice = gTotalPrice + (gObjItem[i].productPrice * parseInt(gObjItem[i].quantity));
            $(".qty_cart").html(totalItem);
            $("#cart-list").append(`
         <div class="row">
         <div class="product-widget col-md-12">
            <div class="product-img col-md-3">
                <img src="${gObjItem[i].productImg}" alt="">
            </div>
            <div class="product-body col-md-4">
                <h3 class="product-name"><a href="#">${gObjItem[i].productName}</a></h3>
            </div>
            <div class="product-body col-md-2">
                <div class="add-to-cart">
                    <div class="qty-label">
                        <div class="input-number">
                            <input type="number" value="${gObjItem[i].quantity}" id="quantity_product">
                            <span class="qty-up">+</span>
                            <span class="qty-down">-</span>
                        </div>
                    </div>
                
                </div>
            </div>
            <div class="product-body col-md-3">
                <h4 class="product-price">$${gObjItem[i].productPrice}.00</h4>
            </div>
            <div class="product-body col-md-1">
                <button class="delete" data-code="${gObjItem[i].pOrderCode}"><i class="fa fa-close"></i></button>
            </div>
          </div>
         </div>`
            );
        }
        var voucherDiv = $(`
        <div class="col-md-12">
        <div class="voucherCode">
            <form>
                <input class="input" type="text" placeholder="Enter voucher code">
                <button class="voucherCode-btn">voucher</button>
            </form>
        </div>
          </div>  
        `);
        $("#cart-list").append(voucherDiv);
        // remove item on cart
        $(".delete").click(function () {
            removeItemOnCart(this);
        });
        // remove item on cart
        function removeItemOnCart(btnParam) {
            var vDataItem = $(btnParam).data();
            var vcode = vDataItem.code;
            for (i = 0; i < gObjItem.length; i++) {
                if (gObjItem[i].pOrderCode === vcode) {
                    gObjItem.splice(i, 1);
                }
            }
            localStorage.setItem("items", JSON.stringify(gObjItem));
            location.reload();
        }
        //show total item selected
        var allItemSelected = $(".qty_cart").html();
        if (!allItemSelected) {
            $("#total_item_slelected").html("");
            $("#sub_total").html("");
        } else {
            $("#total_item_slelected").html(allItemSelected);
            $("#sub_total").html( gTotalPrice + ".000&nbsp;₫");
        }
        // onchange  quantity plus  
     
    }
    // show order item
    function showOderItem(pOderObj) {
        for (let i = 0; i < pOderObj.length; i++) {
            var orderItems = $(`
                 <div class="order-col">
					<div>${pOderObj[i].quantity}x ${pOderObj[i].productName}</div>
					<div>${pOderObj[i].productPrice}.000&nbsp;₫</div>
				</div>
            `);
            $(`.order-products`).append(orderItems);
        }
        $('.order-total').html($('#sub_total').html());
    }
    // quantity plus or minus change 
        $('.input-number').each(function () {
            var $this = $(this),
                $input = $this.find('input[type="number"]'),
                up = $this.find('.qty-up'),
                down = $this.find('.qty-down');
            down.on('click', function () {
                var value = parseInt($input.val()) - 1;
                value = value < 1 ? 1 : value;
                $input.val(value);
            })
            up.on('click', function () {
                var value = parseInt($input.val()) + 1;
                $input.val(value);
            })
        });
  
    // check out button click 
    $("#checkout_btn").click(function () {
        $(`#check_out`).show();
        //getDataCheckOut();
    });
    // submit order button
    $("#submit_order").click(function () {
        getCustomerOrder();
    });
    //get customer order 
    function getCustomerOrder() {
        var vFirstname = $('#fname').val().trim();
        var vLastname = $('#lname').val().trim();
        var vPhone = $('#phone').val().trim();
        var vAddress = $('#address').val().trim();
        var vCity = $('#city').val().trim();
        var vCountry = $('#country').val().trim();
        var vEmail = $('#email').val().trim();
        if (validatation(vFirstname, vLastname, vPhone, vAddress, vCity, vCountry, vEmail)) {
            gCustomer = {
                "lastname": vLastname,
                "firstname": vFirstname,
                "phoneNumber": vPhone,
                "address": vAddress,
                "city": vCity,
                "email": vEmail,
                "country": vCountry
            }
            checkPhoneNumber(vPhone, gCustomer);
        }
    }
    //update customer on database 
    function updateCustomerData(pCustomerData, pId) {
        //call api to update customer
        $.ajax({
            url: "http://localhost:9090/customers/update/" + pId,
            contentType: "application/json",
            type: "PUT",
            data: JSON.stringify(pCustomerData),
            dataType: 'json',
            success: function (resObj) {
                console.log(resObj);
                insertNewOrderData((resObj.id));
            },
            error: function (ajaxContext) {
                console.log(ajaxContext.responseText)
            }
        });
    }
    // inset Customer Customer to database
    function insertNewCustomerData(pCustomerData) {
        $.ajax({
            url: "http://localhost:9090/customers/create",
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(pCustomerData),
            dataType: "json",
            success: function (paramRes) {
                console.log(paramRes.id);
                insertNewOrderData(paramRes.id);
            },
            error: function (error) {
                console.log(error);
            }
        })
    }
    // create new order data by customer id
    function insertNewOrderData(pId) {
        gOrders = {
            "status": "processing",
            "comment": $("#order_comment").val(),
            "amount" : gTotalPrice
        }
        console.log(gOrders);
        //call api to create order by customer Id
        $.ajax({
            url: "http://localhost:9090/orders/create/" + pId,
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(gOrders),
            dataType: "json",
            success: function (resObj) {
                console.log(resObj);
                alert("order created successful !");
                insertnewOrderDetailData(resObj.id);
            },
            error: function (ajaxContext) {
                console(ajaxContext.responseText)
            }
        })
    }
    // add new orderc detail by customerid , order id 
    //orderDetails/{orderId}/{productId}
    function insertnewOrderDetailData(pOrderId) {
      for (let i = 0; i < gObjItem.length; i++) {
          console.log(gObjItem[i].productId);
        orderDetails = {
            "quantityOrder":parseInt(gObjItem[i].quantity),
            "priceEach":gObjItem[i].productPrice
        }
        console.log(orderDetails);
        $.ajax({
            url: "http://localhost:9090/orderDetails/" + pOrderId+"/"+ gObjItem[i].productId,
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(orderDetails),
            dataType: "json",
            success: function (resObj) {
                console.log(resObj);
                alert("order created successful !");
                insertnewOrderDetailData(resObj.id);
            },
            error: function (ajaxContext) {
                console(ajaxContext.responseText)
            }
        })
      }

    }
    //check phone number customer
    function checkPhoneNumber(pPhone, pCustomerObj) {
        //call api get customer by phone number
        $.ajax({
            url: "http://localhost:9090/customers/tel=" + pPhone,
            type: "GET",
            dataType: "json",
            success: function (resObj) { //
                console.log(resObj);
                if (resObj = []) {
                    insertNewCustomerData(pCustomerObj);
                } else {
                    var customerId = resObj.id;
                    updateCustomerData(pCustomerObj, customerId);
                }
            },
            error: function (errorResponse) {
                console.log("dis connected");
            }
        });
    }
    //validate input data
    function validatation(pFname, pLname, pPhone, pAddress, pCity, pCountry, pEmail, pState) {
        try {
            if (pFname === "") { throw "firstname not null , input please !" };
            if (pLname === "") { throw "lastname not null ,input please ! " };
            if (pPhone === "") { throw "phone not null , input please !" };
            if (pAddress === "") { throw "address not null , input please !" };
            if (pCity === "") { throw "city not null , input please !" };
            if (pCountry === "") { throw "country not null , input please !" };
            if(checkEmail(pEmail)===false) {throw "not email style , please input email again "}
            return true;
        } catch (paramError) {
            alert(paramError)
            return false;

        }

    }
    // email input type  like  email input
    function checkEmail(email) {
        var vEmailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (vEmailRegex.test(email)) {
            return true;
        }else
        {
            return false;
        }
    }
});


