$(document).ready(function () {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const gId = urlParams.get('p');
    glimit = 0;
    gOffset = 0;
    var gfilterArray = {
        brand: [],
        color:[],
        size:[],
        strap:[],
        price: {
            min: 1000,
            max: 9999
        }
    }
    var gform = new FormData();
   console.log(gform);
    // check token 
    var gToken = getCookie("accessToken");
        if (gToken!=="") {
            $("#li-login").hide();
            getUserByToken(gToken);
       }
    // check array filter
    if (gfilterArray.brand.length === 0 && gfilterArray.color.length === 0 && gfilterArray.color.length.size==0 ) {
        countProduct();
        loadDataProducts();
    } else {
       // changeArrrayFromData();
        filterProductList();
    }
    // filter  product
    function filterProductList() {
        //call api to filer List
        $.ajax({
            url: "http://localhost:9090/products/productList"+"?pband="+gfilterArray.brand,
            type: "POST",
            processData: false,
            contentType: false,
            data: gfilterArray,
            success: function (data) {
                // loadDataToCard(pRes);
                showProducsToPage(data);
                //countTotalPage(data.length);
                // loadProductToProductCard(pRes);
            },
            error: function (pAjaxContext) {
                console.log(pAjaxContext);
            }
        })
    }
    //get user by  access token 
    function getUserByToken(pToken) {
        $.ajax({
            url: "http://localhost:9090/auth/user/me",
            type: 'GET',
            dataType: "json",
            contentType: "application/json;charset=utf-8",
            headers: {
                "Authorization": "Bearer " + pToken
            },
            success: function (paramResponse) {
                getUserDetail(paramResponse);
            },
            error: function (pAjaxContext) {
                console.log(pAjaxContext.responseText);
            }
        });
    }
    //get users detail 
    function getUserDetail(pUserRespone) {
        console.log(pUserRespone);
        $("#li-fullname").html(`<i class="fa fa-user-o"></i>` + pUserRespone.username);
        var vRole = pUserRespone.authorities[0].authority;
        if (vRole) {

        }
    }
    // show data to page
    function showProducsToPage(pDataObj) {
        for (let i = 0; i < pDataObj.length; i++) {
            var divProduct = $(`<div class="col-md-4 col-xs-6">
        <div class="product">
            <div class="product-img">
                <img src="${pDataObj[i].productImg}" alt="">
            </div>
            <div class="product-body">
                <p class="product-category">Category</p>
                <h3 class="product-name"><a href="productDetail.html?id=${pDataObj[i].id}">${pDataObj[i].productName}</a></h3>
                <h4 class="product-price">$${pDataObj[i].price}.00<del class="product-old-price">$990.00</del></h4>
                <div class="product-rating">
                </div>
                <div class="product-btns">
                    <button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">add to wishlist</span></button>
                    <button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">add to compare</span></button>
                    <button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">quick view</span></button>
                </div>
            </div>
            <div class="add-to-cart">
                <button class="add-to-cart-btn" id="add_cart"><i class="fa fa-shopping-cart"></i> add to cart</button>
            </div>
        </div>
       </div>`);
            $("#all_product").append(divProduct);
        }
    }
    var jsonObj = window.localStorage.getItem("items");
    if (!jsonObj) {
        $(".qty_cart").hide();
        $(".cart-list").hide();
    } else {
        var gObjItem = JSON.parse(jsonObj);
        var totalItem = 0;
        for (let i = 0; i < gObjItem.length; i++) {
            totalItem = totalItem + parseInt(gObjItem[i].quantity);
            $(".qty_cart").html(totalItem);
            $(".cart-list").append(`
            <div class="product-widget">
            <div class="product-img">
                <img src="${gObjItem[i].productImg}" alt="">
            </div>
            <div class="product-body">
                <h3 class="product-name">${gObjItem[i].productName}<a href="#"></a></h3>
                <h4 class="product-price"><span class="qty">${gObjItem[i].quantity}x</span>$${gObjItem[i].productPrice}</h4>
            </div>
          
        </div>`);
        }
    }
    // get All productLine 
    getAllProductLine();
    // get All productLine function
    function getAllProductLine() {
        //call api to get All productLine 
        $.ajax({
            url: "http://localhost:9090/productLines",
            type: "GET",
            dataType: "json",
            success: function (resObj) {
                showProductLines(resObj)
            },
            error: function (errorResponse) {
                console.log("dis connected");
            }
        });
    }
    //count product function
    function countProduct() {
        $.ajax({
            url: "http://localhost:9090/products/count",
            type: "GET",
            dataType: "json",
            success: function (number){
                countTotalPage(number);
            },
            error: function (pAjaxContext) {
                console.log(pAjaxContext.responseText);
            }
        })
    }
    // get cookies 
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    // count total page , show pagr
    function countTotalPage(resParam) {
        var vTotalPage = Math.ceil(resParam / 12);
        var vLeftPage = $(`
    
       <li class="leftPage"><a href="#">
      <i class="fa fa-angle-left">
      </i></a></li>
      `);
        $('.store-pagination').append(vLeftPage);
        for (let i = 0; i < vTotalPage; i++) {
            var pageElement = $(` 
        <li><a href="store.html?p=${i + 1}">${i + 1}</a></li>
        `);
            $('.store-pagination').append(pageElement);
        }
        var vRightPage = $(`
     <li class="rightPage"><a href="">
     <i class="fa fa-angle-right">
     </i></a></li>
     `);
        $('.store-pagination').append(vRightPage);

        $('.store-pagination').on("click", "li.leftPage", loadLeftPage);
        function loadLeftPage() {
            var leftPage = pId - 1;
            console.log(leftPage.parseInt);
            var vLeftPageURL = "store.html"
            urlSiteToOpen = vLeftPageURL + "?" + "p=" + leftPage;
            window.location.href = urlSiteToOpen;
        }
        $('.store-pagination').on("click", "li.rightPage", loadRightPage);
        function loadRightPage() {
            var rightPage = pId + 1;
            var vLeftPageURL = "store.html"
            urlSiteToOpen = vLeftPageURL + "?" + "p=" + rightPage;
            window.location.href = urlSiteToOpen;
            alert(vLeftPageURL);
        }
    }
    // show productline list function 
    function showProductLines(paramObj) {
        for (let i = 0; i < paramObj.length; i++) {
            var divProductLine = $(`<div class="input-checkbox">
        <input type="checkbox" id="${paramObj[i].productLine}" value = "${paramObj[i].productLine}" >
        <label for="${paramObj[i].productLine}">
            <span></span>
             	${paramObj[i].productLine}							
        </label>
          </div>
        `);
            $(".brands").append(divProductLine);
        }
    }
    // load  product like limit offset
    function loadDataProducts() {
        if (!gId || gId === 0 || gId === 1) {
            gOffset = 0;    
            glimit = 12;
        } else {
            gOffset = (gId - 1) * 12;
            glimit = 12 * gId;
        }
        //call api to load all product
        $.ajax({
            url: "http://localhost:9090/products/" + glimit + "/" + gOffset,
            type: "GET",
            dataType: "json",
            success: function (resObj) {
                showProducsToPage(resObj);
            },
            error: function (errorResponse) {
                console.log("dis connected");
            }
        });
        //==================================== fileter ==============================================
        // filter brand check box list
        $("#brand_checkbox_list").on("change", "input", function () {
           inputBrandCheckbox(this);
        })
    }
    // filter products like brand name
    function inputBrandCheckbox(pcheck) {
        if ($(pcheck).prop('checked')) {
            gfilterArray.brand.push($(pcheck).val());
            console.log(gfilterArray, gfilterArray.brand.j);
        } else if ($(pcheck).prop('checked') === false) {
            for (let i = 0; i < gfilterArray.brand.length; i++) {
                if ($(pcheck).val() == gfilterArray.brand[i]) {
                    gfilterArray.brand.splice(i,1);                    
                }
            }
        }
    }
    // check input vendor 
    function inputVendorCheckbox(pcheck) {
        if ($(pcheck).prop('checked')) {
            gfilterArray.brand.push($(pcheck).val());
           console.log(gfilterArray, gfilterArray.brand.j);
        } else if ($(pcheck).prop('checked') === false) {
            for (let i = 0; i < gfilterArray.brand.length; i++) {
                if ($(pcheck).val() == gfilterArray.brand[i]) {
                    gfilterArray.brand.splice(i, 1);
                }
            }

        }
        // filter  button price
        $("#price_filter").click(function () {
            var priceMinInput = parseInt($("#price-min").val());
            var priceMaxInput = parseInt($("#price-max").val());
            priceValueSelection(priceMinInput, priceMaxInput);
        });
        // select Price to filter
        function priceValueSelection(pMin, pMax) {
            console.log(pMin, pMax);
            // add price to array Obj
            gfilterArray.price.min = pMin;
            gfilterArray.price.max = pMax;
            console.log(gfilterArray.price);
            changeArrrayFromData();
        }

        // change array  formData 
        function changeArrrayFromData() {
            //append brand
            gform.append("brand", gfilterArray.brand.join());
            console.log(gform.getAll("brand"));
            //append vendor
            gform.append("vendor", gfilterArray.vendor.join());
            console.log(gform.getAll("vendor"));
            // append to price range
            gform.append("min", gfilterArray.price.min);
            gform.append("max", gfilterArray.price.max);
            // append to 
            gform.append("page", "0");
            gform.append("limit", "12");
        }
    }
});