$(document).ready(function () {
    var gToken = getCookie("accessToken");
    if (gToken !== "") {
        $("#li-login").hide();
        getUserByToken(gToken);
    }
    var gItem = {
        "productId": 0,
        "quantity": 0,
        "pOrderCode": "",
        "productName": "",
        "productPrice": 0,
        "productImg": ""
    }
    var jsonObj = window.localStorage.getItem(`items`);
    console.log("dsdsd");
    // check your Cart
    if (!jsonObj) {
        var gObjItem = [];
        $(".qty_cart").hide();
        $(".cart-list").hide();

    } else {
        var gObjItem = JSON.parse(jsonObj);
        var totalItem = 0;
        for (let i = 0; i < gObjItem.length; i++) {
            totalItem = totalItem + parseInt(gObjItem[i].quantity);
            $(".qty_cart").html(totalItem);
            $(".cart-list").append(`
            <div class="product-widget">
            <div class="product-img">
                <img src="${gObjItem[i].productImg}" alt="">
            </div>
            <div class="product-body">
                <h3 class="product-name">${gObjItem[i].productName}<a href="#"></a></h3>
                <h4 class="product-price"><span class="qty">${gObjItem[i].quantity}x</span>$${gObjItem[i].productPrice}</h4>
            </div>
          
        </div>`);
        }
    }
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const gId = urlParams.get('id');
    getProductDetail(gId);
    // get product details by id
    function getProductDetail(pId) {
        //call api to get product detail by id
        $.ajax({
            url: "http://localhost:9090/products/" + pId,
            type: "GET",
            dataType: "json",
            success: function (resObj) {
                showProductDetail(resObj)
            },
            error: function (errorResponse) {
                console.log("dis connected");
            }
        });
    }
    // get user by token request when token aper
    function getUserByToken(pToken) {
        $.ajax({
            url: "http://localhost:9090/auth/user/me",
            type: 'GET',
            dataType: "json",
            contentType: "application/json;charset=utf-8",
            headers: {
                "Authorization": "Bearer " + pToken
            },
            success: function (paramResponse) {
                getUserDetail(paramResponse);
            },
            error: function (pAjaxContext) {
                console.log(pAjaxContext.responseText);
            }
        });
    }
    //get users detail 
    function getUserDetail(pUserRespone) {
        console.log(pUserRespone);
        $("#li-fullname").html(`<i class="fa fa-user-o"></i>` + pUserRespone.username);
        var vRole = pUserRespone.authorities[0].authority;
        if (vRole) {

        }
    }
    // get cookies 
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    //show productc details
    function showProductDetail(pObj) {
        $("#product_name").html(pObj.productName);
        $("#product_price").html("$" + pObj.price + ".00");
        $("#product_img").attr("src", pObj.productImg);
        $(".add-to-cart-btn").attr("data-id", pObj.id);
        $(".add-to-cart-btn").attr("data-productCode", pObj.productCode);
        $(".add-to-cart-btn").attr("data-productname", pObj.productName);
        $(".add-to-cart-btn").attr("data-productprice", pObj.price);
        $(".add-to-cart-btn").attr("data-img", pObj.productImg);
    }
    // add  product to Cart button
    $(".add-to-cart-btn").click(function () {
        addProductOnCart(this);
    });
    // add  product to Cart function
    function addProductOnCart(btnParam) {
        console.log(btnParam);
        var pData = $(btnParam).data();
        var pDataId = pData.id;
        var pDataCode = pData.productcode;
        var pDataName = pData.productname;
        var pDataPrice = pData.productprice;
        var pDataImg = pData.img;
        gItem = {
            "productId": pDataId,
            "quantity": $("#quantity_product").val(),
            "pOrderCode": pDataCode,
            "productName": pDataName,
            "productPrice": pDataPrice,
            "productImg": pDataImg
        }
        gObjItem.push(gItem);
        localStorage.setItem("items", JSON.stringify(gObjItem));
        location.reload();
    }
});