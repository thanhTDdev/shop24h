
$(document).ready(function () {
    var gResquestSignUp = {
        "username": "",
        "email": "",
        "password": ""
    }
    //check token
    var gToken = getCookie("accessToken");
    if (gToken !== "") {
        $("#li-login").hide();
        getUserByToken(gToken);
    } else {
        $("#li-fullname").hide();
    }
    //check localStorage
    // var jsonObj = window.localStorage.getItem("productOrder");
    // if (!jsonObj) {
    //     $(".cart-list").hide();
    // }
    //load all product
    loadProductTop();
    function loadProductTop() {
        //call api 
        $.ajax({
            url: "http://localhost:9090/products/",
            type: "GET",
            dataType: "json",
            success: function (resObj) {
                showProducsToPage(resObj)
            },
            error: function (errorResponse) {
                console.log("dis connected");
            }
        });
    }
    // show product  top rate to page
    function showProducsToPage(pDataObj) {
        for (let i = 0; i < pDataObj.length; i++) {
            var divProduct = $(`<div class="product">
            <div class="product-img">
                <img src="${pDataObj[i].productImg}" alt="">
            </div>
            <div class="product-body">
                <p class="product-category">Category</p>
                <h3 class="product-name"><a href="#">${pDataObj[i].productName}</a></h3>
                <h4 class="product-price">${pDataObj[i].price} đ<del class="product-old-price">$990.00</del></h4>
                <div class="product-rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
                <div class="product-btns">
                    <button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">add to wishlist</span></button>
                    <button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">add to compare</span></button>
                    <button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">quick view</span></button>
                </div>
            </div>
            <div class="add-to-cart">
                <button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
            </div>
        </div>`);
            $("#top_product").append(divProduct);
        }
    }
    // login user  button click
    $("#btn-login").click(function () {
        var vEmailInputById = $("#email_id").val().trim();
        var vPasswordInputById = $("#password_id").val().trim();
        if (validateRequestLogin(vEmailInputById, vPasswordInputById)) {
            gUserRequest = {
                "email": vEmailInputById,
                "password": vPasswordInputById
            }
            sendRequestObjToLogin(gUserRequest);
        }
    });
    //sign up account click button
    $("#sign-up").click(function () {
        $("#loginModal").hide();
        // signUpNewAccount();
    });
    $("#btn-SignUp").click(function () {
        signUpNewAccount();
    });
    // sign-up  account 
    function signUpNewAccount() {
        var vPasswordInput = $("#password_signup").val().trim();
        var vPasswordConfirmInput = $("#password_signup_confirm").val().trim();
        var vFirstnameInput = $("#fname_id").val().trim();
        var vEmailInput = $("#email_signup").val().trim();
        var vLastnameInput = $("#lname_id").val().trim();
        if (validateRequestSignUp(vFirstnameInput, vLastnameInput, vEmailInput, vPasswordInput, vPasswordConfirmInput)) {
            alert("ok");
            gResquestSignUp = {
                "username": vFirstnameInput + "&nbsp" + vLastnameInput,
                "email": vEmailInput,
                "password": vPasswordInput
            }
            console.log(gResquestSignUp);
            //sendRequestObjToLogin(gResquestSignUp);
        }
    }
    //sign out button click
    $("#sign-out").click(function () {
        signOutAccount();
    });
    // sign out Account
    function signOutAccount() {
        removeItem("accessToken", "/", "localhost");
        window.location.reload(true);
    }
    //send request obj to login form
    function sendRequestObjToLogin(pObjRequest) {
        //call api send request login
        $.ajax({
            url: "http://localhost:9090/auth/login",
            type: 'POST',
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(pObjRequest),
            success: function (pResponseObj) {
                var vToken = pResponseObj.accessToken;
                setCookie("accessToken", vToken, 1);
                $("#loginModal").hide();
                location.reload();
            },
            error: function (pAjaxContext) {
                console.log(pAjaxContext.responseText);
            }
        });
    }
    //remove accessToken function when sign out
    function removeItem(sKey, sPath, sDomain) {
        document.cookie = encodeURIComponent(sKey) +
            "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" +
            (sDomain ? "; domain=" + sDomain : "") +
            (sPath ? "; path=" + sPath : "");
    }
    // get user by token request when token appear
    function getUserByToken(pToken) {
        $.ajax({
            url: "http://localhost:9090/auth/user/me",
            type: 'GET',
            dataType: "json",
            contentType: "application/json;charset=utf-8",
            headers: {
                "Authorization": "Bearer " + pToken
            },
            success: function (paramResponse) {
                getUserDetail(paramResponse);
            },
            error: function (pAjaxContext) {
                console.log(pAjaxContext.responseText);
            }
        });
    }
    //get users detail 
    function getUserDetail(pUserRespone) {
        $("#li-fullname").html(`<i class="fa fa-user-o"></i>` + "&nbsp" + pUserRespone.username);
        getCartByUserId(pUserRespone.id);
        var vRole = pUserRespone.authorities[0].authority;
        if (vRole==="ROLE_ADMIN") {
        //  window.location.href="http://localhost/shop24h/admin%20LTE/";
        }
    } 
    // get cart by user id
    function getCartByUserId(pUserId) {
        //call api to get cart by  userId
        $.ajax({
            url: "http://localhost:9090/Carts/"+pUserId,
            type: 'GET',
            dataType: "json",
            contentType: "application/json;charset=utf-8",
            success: function (paramResponse) {
                 if (paramResponse.length!==0) {
                    var vCartId = paramResponse[0].id;
                    getCartItemHandle(vCartId);
                 } 
            },
            error: function (pAjaxContext) {
                console.log(pAjaxContext.responseText);
            }
        });
    }
    //get cart item handle
    function getCartItemHandle(pCartId) {
        // call api to get cartItems by cartId
        $.ajax({
            url: "http://localhost:9090/CartItems/"+pCartId,
            type: 'GET',
            dataType: "json",
            contentType: "application/json;charset=utf-8",
            success: function (paramResponse) {
                var productIdHandle = paramResponse[0].productId;
                 
            },
            error: function (pAjaxContext) {
                console.log(pAjaxContext.responseText);
            }
        });
    }
    // set  cookies (name , value , time)
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
    // get cookies 
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    // validatetion email , password
    function validateRequestLogin(pEmail, pPassword) {
        try {
            if (checkEmail(pEmail) === false) { throw "not email style , please input email again " }
            if (pPassword === "") { throw "password not null , input please !" };
        } catch (error) {
            alert(error)
            return false;
        }
        return true;
    }
    // validatetion request Sign Up
    function validateRequestSignUp(pFirstname, pLastname, pEmail, pPassword, pPasswordConfirm) {
        try {
            if (pFirstname === "") { throw "firstname not null , input please !" };
            if (pLastname === "") { throw "lastname not null , input please !" };
            if (pEmail === "") { throw "email not null , input  email  please !" }
            else
                if (checkEmail(pEmail) === false) { throw "not email style , please input email again" };
               
            if (pPassword === "") { throw "password not null , input please !" };
            if (pPasswordConfirm !== pPassword) { throw "password confirm incorecct , input again please !" };
        } catch (error) {
            alert(error)
            return false;
        }
    }
    //check email
    function checkEmail(email) {
        var vEmailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (vEmailRegex.test(email)) {
            return true;
        } else {
            return false;
        }
    }
    //check email state 
    function checkEmailState(pEmail) {
        var vCheckEmail = false;
        //call api to check email state
        $.ajax({
            url: "http://localhost:9090/auth/emailState/" + pEmail,
            type: "GET",
            dataType: "json",
            success: function (resObj) {
             if (resObj.length === 1) {
                vCheckEmail === true;
             } else {
                 return vCheckEmail;
             }
            },
            error: function (errorResponse) {
                console.log("dis connected");
            }
        });

    }
});