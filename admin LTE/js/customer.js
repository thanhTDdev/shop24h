
$(document).ready(function () {
    const gModeUpdate = "UPDATE";
    const gModeInsert = "INSERT";
    var gStatus = gModeInsert
    var gCustomerId = 0;
    var gCustomer = {
        "lastname": "",
        "firstname": "",
        "phoneNumber": "",
        "address": "",
        "city": "",
        "state": "",
        "postalCode": "",
        "country": "",
        "salesRepEmployeeNumber": 0,
        "creditLimit": 0
    }
    //show data on table
    showDataOnTable();
    //show data table funtion
    function showDataOnTable() {
        // call api load data to table
        $.ajax({
            url: "http://localhost:9090/customers",
            type: "GET",
            dataType: 'json',
            success: function (resObj) {
                console.log(resObj);
                loadDataOnTable(resObj);
            },
            error: function (error) {
                console.log(error.responseText);
            }
        });
        // load data on table
        function loadDataOnTable(pData) {
            var customerTable = $("#customer-table").DataTable({
                data: pData,
                columns: [ // all obj
                    {
                        data: "id"
                    },
                    {
                        data: "lastname"
                    }, {
                        data: "firstname"
                    }, {
                        data: "phoneNumber"
                    }, {
                        data: "address"
                    },
                    {
                        data: "city"
                    },
                    {
                        data: "state"
                    },
                    {
                        data: "postalCode"
                    },
                    {
                        data: "country"
                    },
                    {
                        data: "salesRepEmployeeNumber"
                    },
                    {
                        data: "creditLimit"
                    }
                    ,
                    {
                        data: "action"
                    }
                ],
                columnDefs: [{
                    targets: -1, // cot cuoi cung 
                    defaultContent: `<div class="row">
<div class="col-sm-3">
    <button class='fas fa-edit text-blue btn btn edit-customer' 
    data-toggle="tooltip" data-placement="bottom" title="Update" ></button>
</div>
&nbsp
<div class="col-sm-3">
    <button class='fas fa-trash-alt text-blue btn btm delete-customer'
     rel="modal:open" data-toggle="tooltip" data-placement="bottom" title="Delete"></button>
</div>
&nbsp
<div class="col-sm-3">
    <button class='fas fa-clipboard text-blue btn btm order-customer'
     rel="modal:open" data-toggle="tooltip" data-placement="bottom" title="My Order"></button>
</div>
</div>
`
                }]
            });
            // edit customer
            $(".edit-customer").click(function () {
                gStatus = gModeUpdate;
                editButtonCustomerDetail(this);
            });
            //delete customer
            $(".delete-customer").click(function () {
                var result = confirm("Want to delete?");
                if (result) {
                    deleteButtonCustomerDetail(this);
                }
            });
            // get my order
            $(".order-customer").click(function () {
                getMyOrder(this);
            });
            // get my order by customer funtion
            function getMyOrder(btnParam) {
                var selectedRow = $(btnParam).parents("tr");
                var dataTableSelectedRow = customerTable.row(selectedRow);
                var dataObj = dataTableSelectedRow.data();
                gCustomerId = dataObj.id;
                const detailFormURL = "order.html"
                urlSiteToOpen = detailFormURL + "?" +
                    "customerId=" + gCustomerId;
                window.location.href = urlSiteToOpen;
            }
            
            //edit customer function
            function editButtonCustomerDetail(btnParam) {
                var selectedRow = $(btnParam).parents("tr");
                var dataTableSelectedRow = customerTable.row(selectedRow);
                var dataObj = dataTableSelectedRow.data();
                gCustomerId = dataObj.id;
                console.log(dataObj);
                
                $("#fname").val(dataObj.firstname);
                $("#lname").val(dataObj.lastname);
                $("#phone_number").val(dataObj.phoneNumber);
                $("#address").val(dataObj.address);
                $("#city").val(dataObj.city);
                $("#state").val(dataObj.state);
                $("#country").val(dataObj.country);
                $("#postal_code").val(dataObj.postalCode);
                $("#sale_rep_employee_number").val(dataObj.salesRepEmployeeNumber);
                $("#credit_limit").val(dataObj.creditLimit);
            }
            //delete voucher data
            function deleteButtonCustomerDetail(btnParam) {
                var selectedRow = $(btnParam).parents("tr");
                var dataTableSelectedRow = customerTable.row(selectedRow);
                var dataObj = dataTableSelectedRow.data();
                gCustomerId = dataObj.id;
                console.log(gCustomerId);
                // call api to delete customer by id
                $.ajax({
                    url: "http://localhost:9090/customers/delete/" + gCustomerId,
                    contentType: "application/json",
                    type: "DELETE",
                    dataType: 'json',
                    success: function (res) {
                        alert("xoa thanh cong");
                    },
                    error: function (ajaxContext) {
                        console.log(ajaxContext.responseText)
                    }
                });
                location.reload();
            }
        }
    }
    //update customer function
    function updateDataCustomer() {
        var gCustomer = {
            "lastname": $("#lname").val(),
            "firstname": $("#fname").val(),
            "phoneNumber": $("#phone_number").val(),
            "address": $("#address").val(),
            "city": $("#city").val(),
            "state": $("#state").val(),
            "postalCode": $("#postal_code").val(),
            "country": $("#country").val(),
            "salesRepEmployeeNumber": $("#sale_rep_employee_number").val(),
            "creditLimit": $("#credit_limit").val()
        }
        console.log(JSON.stringify(gCustomer));
        // call api to update voucher on database
        $.ajax({
            url: "http://localhost:9090/customers/update/" + gCustomerId,
            contentType: "application/json",
            type: "PUT",
            data: JSON.stringify(gCustomer),
            dataType: 'json',
            success: function (resObj) {
                console.log(resObj);
                alert("update thanh cong");
            },
            error: function (ajaxContext) {
                console.log(ajaxContext.responseText)
            }
        });
        location.reload();
    }
    //button add new customer
    $("#add_new").click(function () {

        
    });
    // insert new voucher funtion
    function insertNewCustomer() {
        var vlastname = $("#lname").val().trim();
        var vfirstname = $("#fname").val().trim();
        var vphoneNumber = $("#phone_number").val();
        var vAddress = $("#address").val().trim();
        var vCity = $("#city").val().trim();
        var vState = $("#state").val().trim();
        var vPostalCode = $("#postal_code").val().trim();
        var vCountry = $("#country").val().trim();
        var vSalesRepEmployeeNumber = $("#sale_rep_employee_number").val().trim();
        var vReditLimit = $("#credit_limit").val().trim();
        if (validatation(vlastname,vfirstname,vphoneNumber,vAddress,vCity,vState,vPostalCode,vCountry,vSalesRepEmployeeNumber,vReditLimit)) {
             gCustomer = {
                "lastname": vlastname,
                "firstname": vfirstname,
                "phoneNumber": vphoneNumber,
                "address": vAddress,
                "city": vCity,
                "state": vState,
                "postalCode": vPostalCode,
                "country": vCountry,
                "salesRepEmployeeNumber":vSalesRepEmployeeNumber,
                "creditLimit": vReditLimit
            }
            // call api to insert new voucher on db
            $.ajax({
                url: "http://localhost:9090/voucher/create",
                contentType: "application/json",
                type: "POST",
                data: JSON.stringify(gCustomer),
                dataType: 'json',
                success: function (res) {
                    alert("them thanh cong thanh cong");
                },
                error: function (ajaxContext) {
                    console.log(ajaxContext.responseText)
                }
            });
            location.reload();

        }

    }
    // save data onclick
    $("#save").on("click", saveDataCustomer);
    function saveDataCustomer() {
        if (gStatus === gModeUpdate) {
            //update data voucher         
            updateDataCustomer();
        } else {
            // insert new data  voucher
           // insertNewCustomer();
        }
    }
    //validate input data
    function validatation(pFname, pLname, pPhone, pAddress, pCity, pCountry, pCode, pState, pSaleEmployeeNumber, pCreditLimit) {
        try {
            if (pFname === "") { throw "firstname not null , input please !" };
            if (pLname === "") { throw "lastname not null ,input please ! " };
            if (pPhone === "") { throw "phone not null , input please !" };
            if (pAddress === "") { throw "address not null , input please !" };
            if (pCity === "") { throw "city not null , input please !" };
            if (pCountry === "") { throw "country not null , input please !" };
            if (pCode === "") { throw "postal code not null , input please !" };
            if (pState === "") { throw "sate not null , input please !" };
            if (pSaleEmployeeNumber === "") { throw "sale employee number not null , input please !" };
            if (pCreditLimit === "") { throw "credit limit not null , input please !" };
            return true;
        } catch (paramError) {
            alert(paramError)
            return false;

        }

    }
});
